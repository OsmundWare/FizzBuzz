package cl.ubb.FizzBuzz.test;

import static org.junit.Assert.*;

import cl.ubb.FizzBuzz.FizzBuzz;

import org.junit.Test;

public class FizzBuzzTest {

	

	
	@Test
	public void SiJuegoRecibeUnoRestornaUno()
	{
		
		//arrange
		
		String numero="1";
		String resultado;
		FizzBuzz juego = new FizzBuzz();
		
		//act
			resultado = juego.resultadoJuego(numero);	
		
		//assert
		
			assertEquals("1", resultado);	
		
	}
	
	@Test
	public void JuegoRecibeDosRestornaDos(){
		
		//arrange 
		
		String numero = "2";
		String resultado;
		
		FizzBuzz juego = new FizzBuzz();
		
		//act
		
		resultado = juego.resultadoJuego(numero);
		
		//assert 
		assertEquals("2", resultado);	
	}
	
	@Test
	public void JuegoRecibeTresRetornaFizz(){
		
		//arrange
		
		String numero = "3";
		String resultado;
		
		FizzBuzz juego = new FizzBuzz();
		
		//act
		
		resultado = juego.resultadoJuego(numero);
		
		//assert
		assertEquals("Fizz", resultado);
		
	}
	
	@Test
	public void JuegoRecibeCuatroRetornaCuatro(){
		
		//arrange
		
		String numero = "4";
		String resultado;
		
		FizzBuzz juego = new FizzBuzz();
		
		//act
		
		resultado = juego.resultadoJuego(numero);
		
		//assert
		assertEquals("4", resultado);
		
	}
	
	@Test
	public void JuegoRecibeCincoRetornaBuzz(){
		
		//arrange
		
		String numero = "5";
		String resultado;
		
		FizzBuzz juego = new FizzBuzz();
		
		//act
		
		resultado = juego.resultadoJuego(numero);
		
		//assert
		assertEquals("Buzz", resultado);
		
	}
	
	@Test
	public void JuegoRecibeQuinceRetornaFizzBuzz(){
		
		//arrange
		
		String numero = "15";
		String resultado;
		
		FizzBuzz juego = new FizzBuzz();
		
		//act
		
		resultado = juego.resultadoJuego(numero);
		
		//assert
		assertEquals("FizzBuzz", resultado);
		
	}
	
}
